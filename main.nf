nextflow.enable.dsl=2
// include non-process modules
include {help_message; version_message; complete_message; error_message; pipeline_start_message} from './modules/messages'
include {default_params; check_params } from './modules/params_parser'
include {help_or_version} from './modules/params_utilities'

version = '1.0.0'

// setup default params
default_params = default_params()
// merge defaults with user params
merged_params = default_params + params

// help and version messages
help_or_version(merged_params, version)
final_params = check_params(merged_params)
// starting pipeline
pipeline_start_message(version, final_params)

// include processes for pipelines
include {FILTER_SCAFFOLDS; QUAST; QUAST_SUMMARY; QUAST_MULTIQC; QUALIFYR; QUALIFYR_FAILED_SAMPLE; QUALIFYR_REPORT; WRITE_ASSEMBLY_TO_DIR} from './modules/processes' addParams(final_params)

workflow {
    // set up input data
    sample_id_and_assemblies = Channel
        .fromPath("${params.assembly_dir}/${params.fasta_pattern}")
        .map{ file -> tuple (file.baseName.replaceAll(/\..+$/,''), file)}
        .ifEmpty { error "Cannot find any files matching: ${params.assembly_dir}/${params.fasta_pattern}" }

    sample_id_and_confindr_results = Channel
        .fromPath("${params.confindr_dir}/*.csv")
        .map{ file -> tuple (file.baseName.replaceAll(/_confindr_report$/,''), file)}
        .ifEmpty { error "Cannot find any files matching: ${params.confindr_dir}/*.csv" }
    
    sample_id_and_bactinspector_results = Channel
        .fromPath("${params.bactinspector_dir}/*.tsv")
        .map{ file -> tuple (file.baseName.replaceAll(/\..+$/,''), file)}
        .ifEmpty { error "Cannot find any files matching: ${params.bactinspector_dir}/*.tsv" }
 
    // filter out small and low coverage contigs 
    FILTER_SCAFFOLDS(sample_id_and_assemblies)

    // run quast to assess quality of assemblies
    QUAST(FILTER_SCAFFOLDS.out.scaffolds_for_single_analysis)
    QUAST_SUMMARY(FILTER_SCAFFOLDS.out.scaffolds_for_combined_analysis.collect(sort: {a, b -> a.getBaseName() <=> b.getBaseName()}))
    QUAST_MULTIQC(QUAST.out.quast_dir.collect())

    // summarise quality
    quality_files = QUAST.out.quast_report.join(sample_id_and_confindr_results).join(FILTER_SCAFFOLDS.out.scaffolds_for_single_analysis).join(sample_id_and_bactinspector_results)
    if (final_params.qc_conditions){
        QUALIFYR(final_params.qc_conditions, quality_files)
        QUALIFYR_REPORT(QUALIFYR.out.json_files.collect(), version)
    } else {
        scaffolds = FILTER_SCAFFOLDS.out.scaffolds_for_single_analysis.map{ tuple -> tuple[1]}.collect()
        WRITE_ASSEMBLY_TO_DIR(scaffolds)
    }
}
workflow.onComplete {
    complete_message(final_params, workflow, version)
}

workflow.onError {
    error_message(workflow)
}
