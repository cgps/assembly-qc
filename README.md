# Assembly QC workflow
This [Nextflow](https://www.nextflow.io/) workflow can be used to process fasta files and apply QC
### Authors
Anthony Underwood @bioinformant <au3@sanger.ac.uk>  
Julio Diaz Caballero

## Instructions
The dependencies are provided in a Docker image
```
docker pull registry.gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/assembly:latest
```

Typically the workflow should be run as follows
```
nextflow run main.nf [options] -resume 
```
To run the test set run the following command
  ```
  nextflow run main.nf --input_dir test_input --output_dir test_output --fasta_pattern "*.fasta" --qc_conditions qc_conditions_nextera_relaxed.yml 
  ```

The mandatory options that should be supplied are
  - A source of the fasta files specified as either of the following
    - local files on disk using the `--input_dir` and `--fasta_pattern` arguments
  - The output from the pipeline will be written to the directory specified by the `--output_dir` argument

Optional arguments include
  - `--qc_conditions` Path to a YAML file containing pass/warning/fail conditions used by [QualiFyr](https://gitlab.com/cgps/qualifyr). An example of the format can be seen [here](qc_conditions.yml) and [another](qc_conditions_nextera_relaxed.yml)  more suitable for reads generated from a Nextera library preparation
  - `--skip_quast_summary` Large numbers of assemblies may cause quast summary to hang. Use this parameter to skip this step

## Workflow process
The workflow consists of the following steps

1. Filter the assembly contigs
2. Sumarise all assembly QCs using Quast
3. (Optional if [QuailFyr](https://gitlab.com/cgps/qualifyr) qc conditions YAML file is supplied). Filter assemblies into three directories: pass, warning and failure based on QC  metrics


## Workflow outputs
These will be found in the directory specified by the `--output_dir` argument

  - A directory called `quast` containing
    - A summary quast report named `combined_quast_report.tsv`
    - A transposed summary quast report with the samples as rows so that they can be sorted based on the quast metric in columns named `combined_quast_report.tsv`
  - A directory called `quality_reports` containing html reports
    - [MultiQC](https://multiqc.info/) summary reports combining QC results for all samples from
      - Quast: quast_multiqc_report.html
    - QualiFyr reports. If a qc_conditions.yml file was supplied reports will be generated that contain a summary of the overall pass/fail status of each sample.
      - qualifyr_report.html : e.g [QualiFyr Report](https://glcdn.githack.com/cgps/ghru/pipelines/dsl2/pipelines/assembly/-/raw/develop/README_files/example_qualifyr_report.html)
      - qualifyr_report.tsv

## Software used within the workflow
  - [contig-tools](https://pypi.org/project/contig-tools/) A utility Python package to parse multi fasta files resulting from de novo assembly.
  - [Quast](http://quast.sourceforge.net/quast) A tool to evaluate the aulaity of genome assemblies.
  - [QualiFyr](https://gitlab.com/cgps/qualifyr) Software to give an overall QC status for a sample based on multiple QC metric files
  - [MultiQC](https://multiqc.info/) Aggregate results from bioinformatics analyses across many samples into a single report


