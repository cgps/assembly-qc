include {check_mandatory_parameter; check_optional_parameters} from './params_utilities.nf'

def default_params(){
    /***************** Setup inputs and channels ************************/
    def params = [:] as nextflow.script.ScriptBinding$ParamsMap
    // Defaults for configurable variables
    params.help = false
    params.version = false
    params.assembly_dir = false
    params.confindr_dir = false
    params.bactinspector_dir = false
    params.output_dir = false
    params.minimum_scaffold_length = 500
    params.minimum_scaffold_depth = 3
    params.qc_conditions = false
    params.skip_quast_summary = false
    return params
}

def check_params(Map params) { 
    final_params = params
    // check assembly dir
    final_params.assembly_dir = check_mandatory_parameter(params, 'assembly_dir') - ~/\/$/
    // check confindr dir
    final_params.confindr_dir = check_mandatory_parameter(params, 'confindr_dir') - ~/\/$/
    // check bactinspector dir
    final_params.bactinspector_dir = check_mandatory_parameter(params, 'bactinspector_dir') - ~/\/$/
    // set up output directory
    final_params.output_dir = check_mandatory_parameter(params, 'output_dir') - ~/\/$/

    // make path absolute
    if (final_params.qc_conditions && !final_params.qc_conditions.startsWith("/")){
        final_params.qc_conditions = "${baseDir}/${final_params.qc_conditions}"
    }
    return final_params
}

