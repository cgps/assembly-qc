// filter scaffolds to remove small and low coverage contigs
process FILTER_SCAFFOLDS {
  tag { sample_id }

  input:
  tuple(val(sample_id), path(scaffold_file))

  output:
  tuple(val(sample_id), path("${sample_id}.fasta"), emit: scaffolds_for_single_analysis)
  path("${sample_id}.fasta"), emit: scaffolds_for_combined_analysis
  
  script:
  """
  contig-tools filter -l ${params.minimum_scaffold_length} -c ${params.minimum_scaffold_depth} -f ${scaffold_file}

  if [[ -f "${sample_id}.fasta" ]]
  then
    mv ${sample_id}.fasta ${sample_id}.unfiltered.fasta
  fi
  ln -s *.filter_gt_${params.minimum_scaffold_length}bp_gt_${params.minimum_scaffold_depth}.0cov.fasta ${sample_id}.fasta
  """
}

// assess assembly with Quast
process QUAST {
  tag { sample_id }
    
  publishDir "${params.output_dir}/quast",
    mode: 'copy',
    pattern: "report.tsv",
    saveAs: { file -> "${sample_id}_quast_" + file.split('\\/')[-1] }

  input:
  tuple(val(sample_id), path(contig_file))

  output:
  path("${sample_id}"), emit: quast_dir
  tuple(val(sample_id), path("${sample_id}/report.tsv"), emit: quast_report)

  """
  quast.py ${contig_file} -o .
  mkdir ${sample_id}
  ln -s \$PWD/report.tsv ${sample_id}/report.tsv
  """
}


// assess assembly with Quast but in a single file
process QUAST_SUMMARY {
  tag { 'quast summary' }
  memory { 4.GB * task.attempt }

  publishDir "${params.output_dir}/quast",
    mode: 'copy',
    pattern: "*report.tsv",
    saveAs: { file -> "combined_${file}"}
  
  when:
  ! params.skip_quast_summary

  input:
  path(contig_files)

  output:
  path("*report.tsv") optional true

  """
  quast.py --no-plots --no-html ${contig_files} -o .
  """
}


// QUAST MultiQC
process QUAST_MULTIQC {
  tag { 'multiqc for quast' }
  memory { 4.GB * task.attempt }

  publishDir "${params.output_dir}/quality_reports",
    mode: 'copy',
    pattern: "multiqc_report.html",
    saveAs: { "quast_multiqc_report.html" }

  input:
  path(quast_files) 

  output:
  path("multiqc_report.html")

  script:
  """
  multiqc --interactive .
  """
}


process QUALIFYR {
  tag { sample_id }

  publishDir "${params.output_dir}/assemblies/pass",
    mode: 'copy',
    pattern: 'assemblies/pass/*',
    saveAs: { file -> file.split('\\/')[-1] }

  publishDir "${params.output_dir}/assemblies/warning",
    mode: 'copy',
    pattern: 'assemblies/warning/*',
    saveAs: { file -> file.split('\\/')[-1] }
  
  publishDir "${params.output_dir}/assemblies/failure",
    mode: 'copy',
    pattern: 'assemblies/failure/*',
    saveAs: { file -> file.split('\\/')[-1] }

  input:
  path(qc_conditions_yml)
  tuple(val(sample_id), path(quast_report), path(confindr_report), path(scaffold_file), path(bactinspector_report))

  output:
  path('assemblies/**/*')
  path("${sample_id}.qualifyr.json"), emit: json_files


  """
  # extract min and max genome sizes from bactinspector output, min file size from
  # file_size_check output and replace place holder in conditions file
  MAX_GENOME_LENGTH=\$(cat ${bactinspector_report} | awk -F'\t' 'NR == 2 {print \$8}')
  # if no species match set to 0
  if [ -z \$MAX_GENOME_LENGTH ]; then MAX_GENOME_LENGTH=0; fi
  # add wobble
  MAX_GENOME_LENGTH=\$(echo \$MAX_GENOME_LENGTH  | awk '{printf("%d",  \$1 * 1.1)}')

  MIN_GENOME_LENGTH=\$(cat ${bactinspector_report} | awk -F'\t' 'NR == 2 {print \$9}')
  # if no species match set to 0
  if [ -z \$MIN_GENOME_LENGTH ]; then MIN_GENOME_LENGTH=0; fi
  # add wobble
  MIN_GENOME_LENGTH=\$(echo \$MIN_GENOME_LENGTH  | awk '{printf("%d",  \$1 * 0.9)}')    

  sed -i "s/MAX_GENOME_LENGTH/\${MAX_GENOME_LENGTH}/" ${qc_conditions_yml} 
  sed -i "s/MIN_GENOME_LENGTH/\${MIN_GENOME_LENGTH}/" ${qc_conditions_yml}

  result=`qualifyr check -y ${qc_conditions_yml} -c ${confindr_report}  -q ${quast_report} -b ${bactinspector_report} -s ${sample_id} 2> ERR`
  return_code=\$?
  if [[ \$return_code -ne 0 ]]; then
    exit 1;
  else
    if [[ \$result == "PASS" ]]; then
      qc_level="pass"
    elif [[ \$result == "WARNING" ]]; then
      qc_level="warning"
    elif [[ \$result == "FAILURE" ]]; then
      qc_level="failure"
    fi
    mkdir -p assemblies/\${qc_level}
    mv ${scaffold_file} assemblies/\${qc_level}/

    if [[ \$result != "PASS" ]]; then
      mv ERR assemblies/\${qc_level}/${sample_id}_qc_result.tsv
    fi
  fi


  # make json file
  qualifyr check -y ${qc_conditions_yml} -c ${confindr_report} -q ${quast_report} -b ${bactinspector_report} -s ${sample_id} -j -o .
  """
}


process QUALIFYR_FAILED_SAMPLE {
  tag { sample_id }
  input:
  tuple(val(sample_id), val(file_size))
  tuple(path(quast_template), path(failed_sample_conditions_template), path(bactinspector_template), path(confindr_template), path(fastqc_template), path(file_size_check_template))

  output:
  path("${sample_id}.qualifyr.json")

  script:

  """
  sed -i "s/FILE_SIZE/${file_size}/" ${file_size_check_template}
  sed -i "s/MIN_FILE_SIZE/${params.prescreen_file_size_check}/" ${failed_sample_conditions_template}

  # make json file
  qualifyr check -y ${failed_sample_conditions_template} -f ${fastqc_template} ${fastqc_template} -c ${confindr_template}  -q ${quast_template} -b ${bactinspector_template} -z ${file_size_check_template} -s ${sample_id} -j -o .
  """
}

process QUALIFYR_REPORT {
  tag { 'qualifyr report' }

  publishDir "${params.output_dir}/quality_reports",
    mode: 'copy',
    pattern: "qualifyr_report.*"

  input:
  path(json_files)
  val(version)

  output:
  path("qualifyr_report.*")

  script:
  workflow_command = workflow.commandLine.replaceAll('"', '\\\\"')
//  """
//  qualifyr report -i . -c 'quast.N50,quast.# contigs (>= 0 bp),quast.# contigs (>= 1000 bp),quast.Total length (>= 1000 bp),quast.GC (%),confindr.contam_status,bactinspector.species' -s "Analysis with GHRU Assembly Pipeline version ${version}<br><br>Command line:<br>${workflow_command}"
//  """
  """
  qualifyr report -i . -c 'quast.N50,quast.# contigs,quast.Total length,quast.GC (%),confindr.contam_status,bactinspector.species' -s "Analysis with GHRU Assembly Pipeline version ${version}<br><br>Command line:<br>${workflow_command}"
  """

}

  process WRITE_ASSEMBLY_TO_DIR {
    tag { "assemblies to output" }

    publishDir "${params.output_dir}",
      mode: "copy"

    input:
    path(scaffold_files)

    output:
    path("assemblies")

    script:
    """
    mkdir assemblies
    mv ${scaffold_files} assemblies/
    """

  }
