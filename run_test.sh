NEXTFLOW_WORKFLOWS_DIR='/well/aanensen/shared/nextflow_workflows'
DATA_DIR='/well/aanensen/shared/nextflow_workflows/assembly-qc-1.0.0/test_input'
OUTPUT_DIR='/well/aanensen/shared/nextflow_workflows/assembly-qc-1.0.0/test_output'
/well/aanensen/shared/software/bin/nextflow run \
${NEXTFLOW_WORKFLOWS_DIR}/assembly-qc-1.0.0/main.nf \
--qc_conditions ${NEXTFLOW_WORKFLOWS_DIR}/assembly-2.1.2/qc_conditions_nextera_relaxed.yml \
--assembly_dir ${DATA_DIR}/assemblies \
--confindr_dir ${DATA_DIR}/confindr \
--bactinspector_dir ${DATA_DIR}/bactinspector \
--fasta_pattern '*.fasta' \
--output_dir ${OUTPUT_DIR} \
-w ${DATA_DIR}/work \
-profile bmrc -qs 1000 -resume